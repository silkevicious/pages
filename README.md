# silkevicious.codeberg.page

![License](https://img.shields.io/badge/license-WTFPL-green.svg)

A repo to store all my guides/notes/knowledge/what i learned.
Feel free to contribute, fork, share, whatever, see LICENSE.